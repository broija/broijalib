#!/usr/bin/python3
# coding: utf-8

import pathlib
import subprocess

from .size import str_to_float

def split(path, chunk_size, ext=None):
  if isinstance(chunk_size, str):
    chunk_size = str_to_float(chunk_size)

  path = pathlib.Path(path)
  if path.is_file():
    pargs = ['split', '-b', str(chunk_size), '-d', str(path), str(path.parent / path.stem)]

    if ext:
      pargs.append('--additional-suffix=' + ext)

    subprocess.run(pargs)

# -----

def unsplit(items, output):
  """:param items: path list or wildcard pattern.
     :param output: file or pathlib.Path."""

  pargs = ['cat']

  if isinstance(items, str):
    pargs.append(items)
  else:
    pargs += items

  if isinstance(output, pathlib.Path):
    output = output.open('w')
  print(pargs)
  subprocess.run(pargs, stdout=output)
