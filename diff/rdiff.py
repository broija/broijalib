#!/usr/bin/python3
# coding: utf-8

import subprocess

def rdiff(host, remote_path, local_path, user=None, port=None, ignore_ws=False, remote_left=True):
  """:param ignore_ws: ignore whitespace.
  :param remote_left: by default, remote path is left and local is right. If swap is True, it will be the opposite."""

  print('rdiff: {}:{} {}'.format(host, remote_path, local_path))

  pargs = ['ssh']

  if user:
    host = user + '@' + host

  if port:
    pargs += ['-p', port]

  pargs += [host, 'cat', remote_path]

  sshproc = subprocess.Popen(pargs, stdout=subprocess.PIPE)

  pargs = ['diff']

  if ignore_ws:
    pargs.append('-wbB')

  pargs += ['-', local_path] if remote_left else [local_path, '-']

  proc = subprocess.run(pargs, stdin=sshproc.stdout, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
  sshproc.wait()

  print(proc.stdout.decode())

if __name__ == '__main__':
  import argparse

  argparser = argparse.ArgumentParser()
  argparser.add_argument('host', help='remote host')
  argparser.add_argument('remote_path')
  argparser.add_argument('local_path')

  argparser.add_argument('--port', '-p', help='remote port')
  argparser.add_argument('--user', '-u', help='remote user')

  args = argparser.parse_args()

  rdiff(args.host, args.remote_path, args.local_path, user=args.user, port=args.port)
