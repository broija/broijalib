#!/usr/bin/python3
# coding: utf-8

import pathlib

def choose(choices, message=None, return_index=False):
  """:param choices: can be either str (single choice), list or dict (key: choice, value: choice help)"""

  help_messages = None

  if isinstance(choices, str):
    choices = [choices]
  elif isinstance(choices, dict):
    tmp = choices
    choices = list(tmp.keys())
    help_messages = list(tmp.values())
  else:
    choices = list(choices)

  if not message:
    message = 'Please select an item:'

  count = len(choices)

  if not count:
    return None

  while True:
    print(message)

    index = 0

    for choice in choices:
      help_message = ' (' + help_messages[index] + ')' if help_messages else ""
      print(' - %d : %s%s' % (index, choice, help_message))

      index += 1

    value = input('What is your choice? ')

    try:
      intvalue = int(value)

      if intvalue < 0  or intvalue >= count:
        raise ValueError()

      result = choices[intvalue]

      return result if not return_index else (result, intvalue)

    except Exception:
      print('%s is not a valid choice' % value)

# ------------------------------------------------------------------------------

def yn_question(question):

  while True:
    value = input(question + ' ').lower()

    if value in yn_question.pos_answers:
      return True
    if value in yn_question.neg_answers:
      return False

    print('Invalid answer. Possible answers: %s' % yn_question.answers)

# -----

yn_question.pos_answers = ['y','Y','o','O']
yn_question.neg_answers = ['n','N']
yn_question.answers = yn_question.pos_answers + yn_question.neg_answers

# ------------------------------------------------------------------------------

def path(name, existing=None, check=None):
  """:param name: path name or alias.
  :param existing: True : path must exist, False : path must not exist, None : do not issue an existence check.
  :param check: function that checks path validity."""

  qualifier = 'a'

  if existing==True:
    qualifier = 'an existing'
  elif existing==False:
    qualifier = 'a non-existing'
  elif existring is not None:
    raise TypeError('existing must be either None or bool')

  message = 'Please enter %s path (%s): ' % (qualifier, name)

  while True:
    try:
      path = input(message)

      pathobj = pathlib.Path(path)

      if existing is None:
        return path

      if pathobj.exists() ^ existing:
        raise Exception('path must ' + 'exist' if existing else 'not exist')

      if check and not check(path):
        raise ValueError()

      return path

    except ValueError as ve:
      print('Invalid path (check failed)')

    except Exception as e:
      print('Invalid path: ' + str(e))

def not_empty(message):
  while True:
    result = input(message)

    if len(result) > 0:
      return result

    print('Invalid answer: empty')

# ------------------------------------------------------------------------------

if __name__ == '__main__':
  # some tests
  choose(['a', 'b'], 'Please pick something')
  choose('single choice')
  choose({
    'c': 'some help about c',
    'd': 'a description of d choice'
  }, 'Make a choice:')
