#!/usr/bin/python3
# coding: utf-8

from toolshed.java.wildfly import CLI as WildFlyCLI
from toolshed.java.elytron import Tool as ElytronTool

import toolshed.java.keycloak.vault as vault

_PATH_KEYCLOAK_SERVER = '/subsystem=keycloak-server'
_PATH_SPI = _PATH_KEYCLOAK_SERVER + '/spi='
_PATH_THEME = _PATH_KEYCLOAK_SERVER + '/theme=defaults'

class CLI(WildFlyCLI):
  """Keycloak WildFly CLI helper for docker"""

  def __init__(self, service='keycloak', home='/opt/jboss/keycloak'):
    super().__init__(service=service, home=home)

    self.elytron_tool = ElytronTool(self.service, self.path.parent)

  def install_module(self, plugin, jar_path=None, config=True):
    super().install_module(plugin, jar_path=jar_path)

    if config:
      self.configure_module(plugin)

  def uninstall_module(self, plugin, config=True):
    super().uninstall_module(plugin)

    if not config:
      return

    for spi in plugin.spis:
      for provider in spi.providers:
        self.disable_spi_provider(spi.name, provider.name)

  def configure_plugin(self, plugin):
    if not plugin.has_config:
      return

    for spi in plugin.spis:
      self.add_spi(spi.name)

      for provider in spi.providers:
        self.configure_spi_provider(spi, provider)

  def configure_module(self, plugin):
    self.add_module(plugin.full_name)

    for spi in plugin.spis:
      self.add_spi(spi.name)

      if spi.default_provider:
        self.set_default_spi_provider(spi.name, spi.default_provider)

      for provider in spi.providers:
        self.configure_spi_provider(spi, provider)

  def configure_spi_provider(self, spi, provider):
    self.add_spi_provider(spi.name, provider.name, properties=provider.properties)

    if not self.last_outcome():
      self.add_spi_provider(spi.name, provider.name)

      self.enable_spi_provider(spi.name, provider.name)

      if provider.properties:
        self.set_provider_params(spi.name, provider.name, provider.properties)

    for secret in provider.secrets:
      vp_location = self.get_vault_provider_location(secret.provider)
      if not vp_location:
        print("Failed to retrieve vault provider location")
        return

      vp_secret = self.get_vault_provider_secret(secret.provider)
      if not vp_secret:
        print("Failed to retrieve vault provider secret")
        return

      if not self.elytron_tool.cs_secret_exists(vp_location, secret.name, cs_password=vp_secret):
        self.elytron_tool.cs_add_secret(vp_location, secret.name, cs_password=vp_secret)

  def get_providers(self):
    """Get configuration file providers."""
    return self.run_command(_PATH_KEYCLOAK_SERVER + ':read-attribute(name=providers)')

  def add_provider(self, name):
    """Add provider to configuration file."""
    providers = self.get_providers()

    if name not in providers:
      return self.run_command(
        '{}:list-add(name=providers, index={}, value={})'.format(
          _PATH_KEYCLOAK_SERVER,
          len(providers),
          name
          )
        )

    print('Provider "{}" already exists'.format(name))
    return False

  def remove_provider(self, name):
    """Remove provider from configuration file."""
    providers = self.provider_list()

    index = providers.index(name)

    if index > 0:
      return self.run_command(
        '{}:list-remove(name=providers, index={})'.format(
          _PATH_KEYCLOAK_SERVER,
          index
          )
        )

  def add_module(self, name):
    """Add module to configuration file."""
    return self.add_provider('module:' + name)

  def remove_module(self, name):
    """Remove module from configuration file."""
    return self.remove_provider('module:' + name)

  def theme_cache_enable(self):
    self.run_commands(
      [
        'cd ' + _PATH_THEME,
        ':write-attribute(name=staticMaxAge, value=2592000)',
        ':write-attribute(name=cacheThemes, value=true)',
        ':write-attribute(name=cacheTemplates, value=true)',
      ]
    )

  def theme_cache_disable(self):
    self.run_commands(
      [
        'cd ' + _PATH_THEME,
        ':write-attribute(name=staticMaxAge, value=-1)',
        ':write-attribute(name=cacheThemes, value=false)',
        ':write-attribute(name=cacheTemplates, value=false)',
      ]
    )

  def theme_list_attributes(self):
    return self.ls_path(_PATH_THEME)

  def add_spi(self, name):
    return self.run_command(_PATH_SPI + name + '/:add')

  def add_spi_provider(self, spi, provider, enabled=True, properties=None):
    enabled = self.value_to_str(enabled)

    if properties:
      properties = ",properties=" + self.value_to_str(properties)
    else:
      properties = ''

    return self.run_command('{}{}/provider={}/:add(enabled={}{})'.format(_PATH_SPI, spi, provider, enabled, properties))

  def enable_spi_provider(self, spi, provider):
    return self.run_command('{}{}/provider={}/:write-attribute(name=enabled, value=true)'.format(_PATH_SPI, spi, provider))

  def disable_spi_provider(self, spi, provider):
    return self.run_command('{}{}/provider={}/:write-attribute(name=enabled, value=false)'.format(_PATH_SPI, spi, provider))

  def remove_spi_provider(self, spi, provider):
    return self.run_command('{}:remove'.format(self._get_spi_provider_path(spi, provider)))

  def set_default_spi_provider(self, spi, provider):
    return self.run_command('{}{}/:write-attribute(name=default-provider, value={})'.format(_PATH_SPI, spi, provider))

  def get_provider_param(self, spi, provider, key):
    return self.run_command(
      '{}/:map-get(name=properties, key={})'.format(
        self._get_spi_provider_path(spi, provider),
        key
      )
    )

  def set_provider_param(self, spi, provider, key, value):
    self.spi_provider_set_params(spi, provider, {key: value})

  def set_provider_params(self, spi, provider, params):
    provider_path = self._get_spi_provider_path(spi, provider)

    commands = []
    for key, value in params.items():
      commands.append(
        '{}/:map-put(name=properties, key={}, value={})'.format(
          provider_path,
          key,
          self.value_to_str(value)
        )
      )
    self.run_commands(commands)

  def get_vault_provider_secret(self, provider):
    return self.get_provider_param('vault', provider, 'secret')

  def get_vault_provider_location(self, provider):
    return self.get_provider_param('vault', provider, vault.Vault.name_to_class(provider).LOCATION_ATTRIBUTE_NAME)

  def _get_spi_path(self, name):
    return _PATH_SPI + name

  def _get_spi_provider_path(self, spi, provider):
    return '{}/provider={}'.format(
      self._get_spi_path(spi),
      provider
    )

# -----

if __name__ == '__main__':
  import argparse
  import sys

  class CommandManager:
    def __init__(self):
      self.cli = CLI()

    def connect(self):
      self.cli.connect()

    def provider_list(self):
      return self.cli.get_providers()

    def provider_add(self, name):
      return self.cli.add_provider(name)

    def provider_remove(self, name):
      return self.cli.remove_provider(name)

    def module_add(self, name):
      return self.add_module(name)

    def module_remove(self, name):
      return self.cli.remove_module(name)

    def theme_cache_enable(self):
      self.cli.theme_cache_enable()

    def theme_cache_disable(self):
      self.cli.theme_cache_disable()

    def theme_list_attributes(self):
      return self.cli.theme_list_attributes()

    def spi_add(self, name):
      return self.cli.add_spi(name)

    def spi_provider_add(self, spi, provider):
      return self.cli.add_provider(spi, provider)

    def spi_provider_set_param(self, spi, provider, key, value):
      self.cli.spi_provider_set_params(spi, provider, {key: value})

    def spi_provider_set_params(self, spi, provider, params):
      provider_path = self._get_spi_provider_path(spi, provider)

      commands = []
      for key, value in params.items():
        commands.append(
          '{}/:map-put(name=properties, key={}, value={})'.format(
            provider_path,
            key,
            self.cli.value_to_str(value)
          )
        )
      self.cli.run_commands(commands)

# -----

  argparser = argparse.ArgumentParser(
    description="Keycloak configuration handling using WildFly CLI."
              + " If no command is supplied, a connection to WildFly CLI will be established.")

  subparsers = argparser.add_subparsers(title='commands')

  theme_parser = subparsers.add_parser('theme', help='Theme commands')
  theme_group = theme_parser.add_mutually_exclusive_group(required=True)
  theme_group.add_argument('--disable-cache', dest='theme_cache_disable', action='store_true')
  theme_group.add_argument('--enable-cache', dest='theme_cache_enable', action='store_true')
  theme_group.add_argument('--list-attributes', dest='theme_list_attributes', action='store_true')

  provider_parser = subparsers.add_parser('provider', help='Provider commands')
  provider_group = provider_parser.add_mutually_exclusive_group(required=True)
  provider_group.add_argument('--list', '-l', dest='provider_list', action='store_true')
  provider_group.add_argument('--add', '-a', dest='provider_add')
  provider_group.add_argument('--remove', dest='provider_remove')
  provider_group.add_argument('--module-add', '-m', dest='module_add')
  provider_group.add_argument('--module-remove', dest='module_remove')

  execute_parser = subparsers.add_parser('execute', help='Custom commands')
  execute_parser.add_argument('arguments', metavar='ARG', nargs='+', help='Run custom WildFly CLI command.')

  args = argparser.parse_args()
  print(args)

  manager = CommandManager()

  if not len(args.__dict__):
    manager.connect()
    sys.exit(0)

  for arg, value in args.__dict__.items():
  #   print(arg, value)

    if not value:
      continue

    if value == True:
      print(getattr(manager, arg)())
      continue

    print(getattr(manager, arg)(value))
