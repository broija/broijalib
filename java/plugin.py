# coding: utf-8

import re
import xml.etree.ElementTree as xmlet

class Plugin:
  def __init__(self, path, is_module=False, install=False, full_name=None):
    self.path = path
    self.name = path.name
    self.is_module = is_module

    self.install = install
    self.dependencies = None

    self.group_id = None
    self.artifact_id = None
    self.version = None

    self.jar_name = None

    # pom.xml
    pom_path = self.path / 'pom.xml'

    pom_root = xmlet.parse(str(pom_path)).getroot()

    # retrieving root namespace
    match_res = self.__class__.ns_regex.match(pom_root.tag)
    ns = {'maven': match_res.group(1)}

    self.artifact_id = pom_root.find('maven:artifactId', ns).text

    self._xml_set_text('group_id', pom_root, 'maven:groupId', ns)
    self._xml_set_text('version', pom_root, 'maven:version', ns)

    print("ARTIFACTID[{}] GROUPID[{}] VERSION[{}]".format(self.artifact_id, self.group_id, self.version))

    if full_name:
      self.full_name = full_name
    else:
      self.full_name = self.artifact_id

      if self.group_id:
        self.full_name = self.group_id + '.' + self.full_name

    self._xml_set_text('jar_name', pom_root, 'maven:build/maven:finalName', ns)
    if not self.jar_name:
      self.jar_name = self.name

      if self.version:
        self.jar_name = self.jar_name + '-' + self.version

    self.jar_name = self.jar_name + '.jar'

    # maven dependencies: unused because of org.jboss.logging declaration
    # seems not to be consistent between maven and jboss-deployment-structure which keycloak is using
#     dependencies = root.findall('*/maven:dependency', ns)
#     if dependencies:
#       self.dependencies = []

#       for dep in dependencies:
#         self.dependencies.append(
#           dep.find('maven:groupId', ns).text + '.' + dep.find('maven:artifactId', ns).text
#         )

    # if java code is included
    if (self.path / 'src/main/java').is_dir():
      # jboss-deployment-structure.xml: dependencies
      jds_path = self.path / 'src/main/resources/META-INF/jboss-deployment-structure.xml'

      jds_root = xmlet.parse(str(jds_path)).getroot()

      dependencies = jds_root.findall('**/module')
      if dependencies:
        self.dependencies = []

        for dep in dependencies:
          self.dependencies.append(
            dep.attrib["name"]
          )
      print('Dependencies:', ','.join(self.dependencies))

  def _xml_set_text(self, var, parent, xpath, ns):
    element = parent.find(xpath, ns)

    if element is not None:
      setattr(self, var, element.text)

Plugin.ns_regex = re.compile('{(.*)}.*')
