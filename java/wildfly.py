#!/usr/bin/python3
# coding: utf-8

"""WildFly / JBoss CLI helper for docker"""

import json
import pathlib
import re

from toolshed.docker.compose import Compose

class CLI:
  class ConnectionFailureError(Exception): pass

  def __init__(self, service, home):
    """:param service: docker-compose service name."""

    if isinstance(home, str):
      home = pathlib.Path(home)

    self.home = home
    self.path = home / 'bin/jboss-cli.sh'
    self.compose = Compose()
    self.service = service

    self._last_outcome = None

  def last_outcome(self):
    return self._last_outcome

  def connect(self):
    self._exec(['--connect'])

  def run_command(self, *args, decode=True):
    return self._exec(['--connect', '--output-json', *args], follow=False, decode=decode)

  def run_commands(self, commands):
    self.run_command(','.join(commands), decode=False)

  def write_attribute(self, path, name, value):
    self.run_command(
      '{}:write-attribute(name={}, value={}'.format(
        path,
        name,
        self.value_to_str(value)
        )
      )

  def write_attributes(self, path, attributes):
    commands = []

    for name, value in attributes.items():
      commands.append(
        '{}:write-attribute(name={}, value={}'.format(
          path,
          name,
          self.value_to_str(value)
        )
      )

    if commands:
      self.run_commands(commands)

  def ls_path(self, path):
    return self.run_command('ls ' + path)

  def install_module(self, plugin, jar_path=None):
    if not jar_path:
      jar_path = plugin.path / 'target' / plugin.jar_name

    self._module('add', plugin, resources=jar_path, dependencies=True)

  def uninstall_module(self, plugin):
    self._module('remove', plugin)

  def _module(self, command, plugin, resources=None, dependencies=False):
    command = '--command=module {} --name={}'.format(
        command,
        plugin.full_name
    )

    if resources:
      command += ' --resources={}'.format(resources)

    if dependencies:
      command += ' --dependencies=' + ','.join(plugin.dependencies)

    self._exec([command])

  def _exec(self, args, follow=True, decode=True):
    res = self.compose.exec([self.service, str(self.path)] + args, follow=follow)

    if decode and not follow:
      return self._decode(res)

  def _decode(self, response):
    if response[0] != '{':
      if response.startswith('Failed to connect'):
        raise self.__class__.ConnectionFailureError()

    json_res = json.loads(response)

    self._last_outcome = json_res['outcome'] == 'success'

    print(json_res)
    if 'result' in json_res:
      return json_res['result']

    return self._last_outcome

  def _replace_colon(self, string):
    return string.replace(':', '__colon__')

  def value_to_str(self, value):
    if value is False:
      return 'false'
    if value is True:
      return 'true'

    if type(value) is dict:
      tmp = {}

      for k, v in value.items():
        if type(v) is str:
          v = self._replace_colon(v)
        elif type(v) is list:
          v = [self._replace_colon(item) for item in v]

        tmp[k] = v

      value = tmp

      return json.dumps(value).replace(':', '=>').replace('__colon__', ':') .replace('["','[').replace('","',',').replace('"]',']')

    if type(value) is list:
      return '[' + ','.join(value) + ']'

    return value

if __name__ == '__main__':
  import argparse

  class UnknownCommandError(Exception): pass

  valid_commands = [
    'connect'
  ]

  argparser = argparse.ArgumentParser()
  argparser.add_argument('command', default='connect', nargs='?')
  argparser.add_argument('arguments', nargs='*')

  args = argparser.parse_args()

  if args.command not in valid_commands:
    raise UnknownCommandError(args.command)

  cli = CLI()
  getattr(cli, args.command)(*args.arguments)
