#!/usr/bin/python3
# coding: utf-8

import getpass
import subprocess

import toolshed.platform.windows

def mount(path, user=None, drive=None):
  if not drive:
    drive = toolshed.platform.windows.first_free_drive()

  drive = toolshed.platform.windows.drive(drive)
  
  path = str(path).replace('/','\\')

  pargs = ['NET', 'USE', drive, path]
  
  if user:
    user = user.replace('/', '\\')
    passwd = getpass.getpass('Please enter password for "{}@{}": '.format(user, path))
    pargs += [passwd, '/user:' + user]

  # print(pargs)

  try:
    proc = subprocess.run(pargs)

  except CalledProcessError as cpe:
    # hiding password if needed
    if user:
      cpe.cmd = None

    raise cpe
  
  if proc.returncode == 0:  
    return drive

# -----
  
def dismount(drive):
  drive = toolshed.platform.windows.drive(drive)

  subprocess.run(['NET', 'USE', '/DELETE', drive])

# -----

class MountManager:
  def __init__(self, path, user=None, drive=None):
    self.path = path
    self.user = user
    self.drive = drive
  
  def __enter__(self):
    self.drive = mount(self.path, user=self.user, drive=self.drive)

    return self

  def __exit__(self):
    dismount(self.drive)
