#!/usr/bin/python3
# coding: utf-8

import os
import subprocess

class MountError(Exception): pass

def mount(source, mount_point, user=None):
  if not isinstance(source, str):
    source = str(source)

  print(source)

  if source.startswith('sshfs://'):
    source = source[8:]
    print(source)

  if user:
    source = "{}@{}".format(user, source)

  if not os.path.exists(mount_point):
    os.makedirs(mount_point)

  pargs = ['sshfs', source, mount_point]
  print(pargs)
  proc = subprocess.run(pargs)

  if proc.returncode != 0:
    raise MountError(source)

# -----

def unmount(mount_point):
  pargs = ['fusermount', '-u', mount_point]
  subprocess.run(pargs)

dismount = unmount

# -----

def is_mounted(mount_point):
  pargs = ['mountpoint', '-q', mount_point]
  proc = subprocess.run(pargs)

  return proc.returncode == 0

# ----- -----

class MountManager:
  def __init__(self, source, mount_point, user=None):
    self.source = source
    self.mpoint = mount_point
    self.user = user
    self.dismount = self.unmount

  def mount(self):
    mount(self.source, self.mpoint, user=self.user)
    self.mounted = True

  def unmount(self):
    unmount(self.mpoint)
    self.mounted = False

  def is_mounted(self):
    return is_mounted(self.mpoint)

  def __enter__(self):
    self.mount()
    return self

  def __exit__(self, *args, **kwargs):
    self.unmount()
