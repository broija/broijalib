#!/usr/bin/python3
# coding: utf-8

import os.path
import string
import subprocess

from .platform import mingw_path_to_platform_path

_console_encoding = 'cp437'
# _console_encoding = 'cp65001'

# -----

_DRIVE_LETTERS = list(string.ascii_uppercase)

# -----

def drive_is_used(letter):
  proc = subprocess.run(['CMD', '/C', 'WMIC', 'logicaldisk', 'get', 'name'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
  
  used_drives = proc.stdout.decode(_console_encoding).split()[1:]
  
  letter = letter.upper()

  if not letter.endswith(':'):
    letter += ':'

  return letter in used_drives

# -----

def is_drive_letter(letter):
  return len(letter) == 1 and letter.upper() in _DRIVE_LETTERS

# -----

def drives():
  return _DRIVE_LETTERS

# -----
  
def used_drives():
  return [drive for drive in _DRIVE_LETTERS if drive_is_used(drive)]

# -----

def first_free_drive(start='C'):
  if not is_drive_letter(start):
    start='A'

  start = start.upper()

  for drive in _DRIVE_LETTERS:
    if drive < start:
      continue

    if not drive_is_used(drive):
      return drive
      
# -----

def drive(letter):
  if not letter[-1] == ':':
    letter += ':'

  return letter
  
# -----

def get_console_encoding():
  return _console_encoding