#!/usr/local/bin/python3.7
# coding: utf-8

import datetime
import os
import pathlib
import subprocess

import toolshed.prompt
import toolshed.log.tee

class Builder:
  """configure / make manager."""
  class ConfigureNotFound(FileNotFoundError): pass
  class Failure(Exception): pass

  def __init__(self, log_dir_path=None, dryrun=False):
    self.dryrun = dryrun
    
    self.configure_path = None
    
    if log_dir_path:
      self.log_dir_path = pathlib.Path(log_dir_path).resolve(strict=True)
# -----
  def __run(self, pargs, env=None, log_filename=None):
    """Runs a subprocess with pargs arguments. If defined, records a log file.
       :param: env environment variables."""
    if self.dryrun:
      pargs = ['echo'] + pargs

    print(pargs)

    log_path = None

    if log_filename:
      log_path = self.log_dir_path / log_filename

      log_path = log_path.parent / (log_path.stem + datetime.datetime.now().strftime('.%Y%m%d_%H%M%S') + log_path.suffix)
    
    with toolshed.log.tee.Tee(log_path=log_path,mode='ab') as tee:
      compl_proc = subprocess.run(pargs, env=env)
    
    if compl_proc.returncode:
      raise Builder.Failure(' '.join(pargs))
# -----
  def configure(self, source_path, build_path=None, cpp=None, cc=None, cxx=None, options=None, prefix=None):
    """Searches for configure scripts in source path. Prompts user to choose if necessary.
       Runs in build_path.
       :param: cpp preprocessor
       :param: cc C compiler
       :param: cxx C++ compiler"""
    if not self.configure_path:
      self.source_path = pathlib.Path(source_path).resolve(strict=True)
  
      configure_scripts = []
      
      for script in self.source_path.glob('**/configure'):
        configure_scripts.append(script)
  
      if not configure_scripts:
        raise Builder.ConfigureNotFound('configure script not found!')
  
      count = len(configure_scripts)
  
      if count == 1:
        script = configure_scripts[0]
      else:
        script = toolshed.prompt.choose(configure_scripts,message='Choose a configure script:')
        
      self.configure_path = script.resolve()

    if not build_path:
      build_path = source_path
 
    self.build_path = pathlib.Path(build_path).resolve()

    if not self.build_path.is_dir():
      os.makedirs(self.build_path)

    os.chdir(self.build_path)

    tmpenv = {}
    configure_env = None

    if cpp:
      tmpenv['CPP'] = cpp

    if cc:
      tmpenv['CC'] = cc

    if cxx:
      tmpenv['CXX'] = cxx
      
    if tmpenv:
      # updating environment variables for our subprocess
      configure_env = {**os.environ, **tmpenv}

    pargs = [self.configure_path]
    
    if options:
      options = options.split(' ')
              
      for option in options:
        pargs.append(option)
      
    if prefix:
      prefix = pathlib.Path(prefix).resolve()
    
      pargs.append('--prefix=' + str(prefix))
      
    self.__run(pargs, log_filename='configure.log', env=configure_env)
# -----
  def make(self, target=None, jobs=None):
    pargs = ['make']
    
    if target:
      pargs += [target]

    if isinstance(jobs,int) and jobs >= 1:
      pargs += ['-j',str(jobs)]
      
    self.__run(pargs)
# -----
  def test(self, options=None, env=None):
    """:param: env environment variables."""
    pargs = ['make','test']

    if options:
      options = options.split(' ')

      for option in options:
        pargs += [option]

    self.__run(pargs, env)
# -----
  def clean(self):
    self.make('clean')
# -----
  def install(self):
    self.make('install')
# -----
  def altinstall(self):
    self.make('altinstall')
# -----