#!/usr/bin/python3
# coding: utf-8

import subprocess
import sys
import threading

# source: https://stackoverflow.com/a/53312631
class Interactive:
  def __init__(self, env=None, encoding='utf-8'):
    self.env = env
    self.encoding = encoding

  def run(self, pargs):
    """:param pargs: process args."""
    self.proc = subprocess.Popen(pargs, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=self.env)

    writer = threading.Thread(target=self._writeall)
    writer.start()

    while True:
      data = sys.stdin.read(1)

      if not data:
        # ctrl + D
        if data == '':
          self.proc.terminate()

        break

      self.proc.stdin.write(data.encode())
      self.proc.stdin.flush()

    self.proc.returncode

  def _writeall(self):
    data = None

    while True:
      tmp = self.proc.stdout.read(1)
#      data = self.proc.stdout.read(1)

      if data is None:
        data = tmp
      else:
        data += tmp
#      print(data)
      try:
        decoded = data.decode(self.encoding)
        data = None
      except:
        pass

      if not decoded:
          break

      sys.stdout.write(decoded)
      sys.stdout.flush()
