# coding: utf-8

import getpass

def get(message, confirm=False):
  try:
    while True:
      passwd = getpass.getpass(message + ': ')
    
      if not confirm or passwd == getpass.getpass('Confirm your password: '):
        return passwd
  
      print("Password mismatch.")
  except:
    # Hiding real error to avoid password appearance in the clear
    raise Exception('Error while asking for password...')
