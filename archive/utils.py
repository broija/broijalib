#!/usr/bin/python3
# coding: utf-8

import pathlib

def strip_exts(archive_path):
  """Return file name without archive extensions."""

  archive_path = pathlib.Path(archive_path)

  result = archive_path.name

  suffixes = archive_path.suffixes

  suffixes.reverse()

  for suffix in suffixes:
    if suffix in ['.bz2','.gz','.tar','.tgz','.xz','.zip']:
      result = result.rstrip(suffix)
    else:
      break
      
  return result