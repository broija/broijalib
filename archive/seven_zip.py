#!/usr/bin/python3
# coding: utf-8

import shutil
import subprocess

import toolshed.password

_DEFAULT_BIN_PATH = '/usr/local/bin/7zzs'

class SevenZip:
    def __init__(self, bin_path=_DEFAULT_BIN_PATH, remember_password=False):
        if not bin_path:
            bin_path = _DEFAULT_BIN_PATH
        self.bin_path = bin_path

        self._passwd = None
        self._remember_passwd = remember_password

    def add(self, archive_path, target_path, password=None, level=None, volume_size=None, rsync_filter_path=None):
        print('7z add {} {}'.format(archive_path, target_path))
        pargs = []
        self.archive_path = archive_path

        if rsync_filter_path:
            pargs += self._rsync_filter_path_to_args(rsync_filter_path)
        if volume_size:
            pargs.append('-v'+volume_size)
        if level:
           pargs.append('-mx={}'.format(level))

        pargs += ['-t7z', '-mhe', '-mhc', self.archive_path, target_path]

        self._run('a', pargs, password=password)

    def extract(self, archive_path, output_dir, password=None):
        print('7z extract {} -> {}'.format(archive_path, output_dir))
        self.archive_path = self.get_archive_1st_file_path(archive_path)
        self._run('x', ['-o'+str(output_dir), self.archive_path], password=password)

    def list(self, archive_path, password=None):
        self.archive_path = archive_path
        self._run('l', ['-slt', self.archive_path], password=password)

    def get_archive_1st_file_path(self, path):
        return path if path.is_file() else path.with_suffix(path.suffix + '.001')

    def copy(self, archive_path, dest_path):
        print('Copying {} to {}...'.format(archive_path, dest_path))
        for item in archive_path.parent.glob('*' + archive_path.name + '*'):
            shutil.copy2(item, dest_path)

    def _run(self, command, args, password=None):
        args = [self.bin_path, command] + args
        if password:
            if not self._passwd:
                if True == password:
                    password = self._password_prompt('a' == command)

            if self._remember_passwd and not self._passwd:
                self._passwd = password

            args.append('-p{}'.format(self._passwd))

        print(args)
        subprocess.run(args)

    def _rsync_filter_path_to_args(self, path):
        args = None

        with open(path, 'r') as filter_file:
            args = []
            while True:
                line = filter_file.readline()
                if not line:
                    break

                line = line.strip()
                if not line:
                    continue
                recurse = 'r' if '/' not in line else ""
                wildcard = line[2:]
                switch = None
                if '-' == line[0]:
                    switch = 'x'
                elif '+' == line[0]:
                    switch = 'i'

                if switch:
                    args += ['-{}{}!{}'.format(switch, recurse, wildcard)]

        return args

    def _password_prompt(self, confirm):
        return toolshed.password.get('Enter your password for archive {}'.format(self.archive_path), confirm=confirm)
