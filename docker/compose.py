#!/usr/bin/python3
# coding: utf-8

import os
import pathlib
import subprocess

import toolshed.subprocess.interactive

class Compose:
  class InvalidCommandError(Exception): pass
  class InvalidServiceError(Exception): pass

  def __init__(self, path=None, bin_path=None, encoding='utf-8'):
    """:param path: path to docker-compose.yml
       :param bin_path: path to docker-compose binary"""

    self.files = []

    if path:
      self.add_file(path)

    self.bin = bin_path if bin_path else 'docker-compose'

    self.encoding = encoding

    # 'start' alias for 'up'
    # self.start = self.up

    self.project_dir = None

    self.print_commands = True
    self.print_output = True

    self._last_returncode = None
#     self._last_recorded_output = None

  def toggle_print_commands(self, state):
    self.print_commands = state

  def toggle_print_output(self, state):
    self.print_output = state

  def add_file(self, path):
    self.files.append(path)

  def load_compose_files(self, path):
    """Load compose file paths from a list.
       If paths are relative ones, they must be expressed relatively to path list parent directory."""

    with path.open('r') as list_file:
      while True:
        line = list_file.readline().strip()

        if not line:
          break

        compose_file_path = pathlib.Path(line)

        if not compose_file_path.is_absolute():
          compose_file_path = path.parent / compose_file_path

        self.add_file(compose_file_path)

  def set_project_dir(self, path):
    self.project_dir = path

  def _append_services(self, pargs, services):
    if services:
      if isinstance(services, str):
        services = [services]

      pargs += services

    return pargs

  def _compose(self, args, pass_files=True, follow=False, interactive=False, print_output=None, return_output=False):
    retcode = 0

    pargs = [self.bin]

    if self.project_dir:
      pargs += ['--project-directory', str(self.project_dir)]

    if pass_files:
      for f in self.files:
        pargs += ['-f', str(f)]

    pargs += args

    if self.print_commands:
      print(pargs)

    if print_output is None:
      print_output = self.print_output

    if not follow and not interactive:
      proc = subprocess.run(pargs, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

      self._last_returncode = proc.returncode

      result = proc.stdout.decode(self.encoding)

      if print_output:
        print(result)

#       self._last_recorded_output = result
      return result

    else:
      if interactive:
        env = os.environ.copy()
        env['COMPOSE_INTERACTIVE_NO_CLI'] = '1'

        proc = toolshed.subprocess.interactive.Interactive(env=env)

        return proc.run(pargs)

      else:
        proc = subprocess.Popen(pargs, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        chars = b""
        complete_output = ""

        while True:
          chars += proc.stdout.read(1)

          if not chars:
            break

          try:
            output = chars.decode(self.encoding)
            if return_output:
              complete_output += output

            chars = b""
            print(output, end='', flush=True)
          except Exception as e:
            # if an Exception occured while decoding try again unless we've reached 10 char length
            if len(chars) > 10:
              raise e

        self._last_returncode = proc.wait()

        if return_output:
#           self._last_recorded_output = complete_output
          return complete_output

        return self._last_returncode

#        if sleep:
#          time.sleep(sleep)

  def build(self, args=None):
    pargs = ['build']

    if args:
      pargs += args

    self._compose(pargs)

  def enter(self, service):
    if not service:
      raise self.InvalidServiceError('Empty service')

    if isinstance(service, list):
      service = service[0]

    self.exec([service, '/bin/bash'], interactive=False)

  def exec(self, args, follow=True, interactive=False, print_output=None, return_output=False, no_tty=False):
    pargs = ['exec']

    if no_tty:
      pargs.append('-T')

    pargs += [*args]

    return self._compose(pargs, follow=follow, interactive=interactive, print_output=print_output, return_output=return_output)

  def logs(self, services=None, follow=True):
    pargs = ['logs']

    if follow:
      pargs.append('-f')

    pargs = self._append_services(pargs, services)

    self._compose(pargs, follow=follow)

  def get_container_id(self, service):
    return self.ps(['-q', service]).strip()

  def ps(self, options=None, services=None, quiet=False):
    pargs = ['ps']

    if options:
      pargs += options

    if quiet:
      pargs.append('-q')

    pargs = self._append_services(pargs, services)

    return self._compose(pargs)

  def restart(self, args=None):
    pargs = ['restart']

    if args:
      pargs += args

    self._compose(pargs)

  def run(self, service, args=None, entrypoint=None, rm=False):
    pargs = ['run']

    if args:
      pargs += args

    if rm:
      pargs.append('--rm')

    if entrypoint:
      pargs += ['--entrypoint', entrypoint]

    pargs.append(service)

    self._compose(pargs)

  def start(self):
    self._compose(['start'])
    return self.get_last_returncode()

  def stop(self):
    self._compose(['stop'])
    return self.get_last_returncode()

  def up(self, detached=True):
    args = ['up']

    if detached:
      args.append('-d')

    self._compose(args)
    return self.get_last_returncode()

  def down(self):
    self._compose(['down'])
    return self.get_last_returncode()

  def command(self, command, args):
    valid_commands = self.__class__._valid_commands

    if not command in valid_commands:
      raise self.__class__.InvalidCommandError('Valid commands: {}', valid_commands)

    if valid_commands[command]:
      getattr(self, command)(args)
    else:
      getattr(self, command)()

  def get_last_returncode(self):
    return self._last_returncode

  def file_exists(self, service, path):
    self.exec([service, 'stat', str(path)], follow=False, print_output=False)

    return self.get_last_returncode() == 0

  @classmethod
  def valid_commands(cls):
    return list(cls._valid_commands.keys())

# key = command, value = args expected
Compose._valid_commands = {'build': True, 'down': False, 'enter': True, 'exec': True, 'logs': True, 'ps': True, 'restart': False, 'run': True, 'start': False, 'stop': False, 'up': False}

# ------------------------------------------------------------------------------

if __name__ == '__main__':
  import argparse

  compose = Compose()

  argparser = argparse.ArgumentParser()
  argparser.add_argument('command', help='Valid commands: {}'.format(compose.valid_commands()))
  argparser.add_argument('command_args', nargs='*')
  argparser.add_argument('--project-directory', '-p')
  argparser.add_argument('--compose-list-path', '-l', type=pathlib.Path, help='Loaded before --compose-files argument.')
  argparser.add_argument('--compose-files', '-f', metavar='compose_file', nargs='+')

  args = argparser.parse_args()

  if args.project_directory:
    compose.set_project_dir(args.project_directory)

  if args.compose_list_path:
    compose.load_compose_files(args.compose_list_path)

  if args.compose_files:
    for f in args.compose_files:
      compose.add_file(f)

  compose.command(args.command, args.command_args)
