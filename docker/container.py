# coding: utf-8

import subprocess

class Container:
  def __init__(self, id):
    self.id = id

  def copy_from_host(self, source, dest, archive=True):
    pargs = ['docker', 'cp']

    if archive:
      pargs.append('-a')

    pargs += [str(source), self.id + ':' + str(dest)]
    subprocess.run(pargs)

  def copy_to_host(self, source, dest, archive=True):
    pargs = ['docker', 'cp']

    if archive:
      pargs.append('-a')

    pargs += [self.id + ':' + str(source), str(dest)]

    subprocess.run(pargs)

# -----

class Temp(Container):
  """Docker temporary container."""

  def __init__(self, image):
    self.image = image

  def __enter__(self):
    proc = subprocess.run(['docker', 'create', self.image], stdout=subprocess.PIPE)
    self.id = proc.stdout.decode().strip()

    print('Created temp container "{}"'.format(self.id))

    return self

  def __exit__(self, *args, **kwargs):
    if self.id:
      proc = subprocess.run(['docker', 'rm', self.id], stdout=subprocess.PIPE)
